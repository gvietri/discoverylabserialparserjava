package parse;

public class Trace {

	private boolean traceOn;
	public void setTrace(boolean t)
	{
		traceOn = t;
	}
	public Trace(boolean on)
	{
		traceOn = on;
	}
	
	public void trace(String m)
	{
		if(traceOn)
			System.out.print(m);
	}
}
