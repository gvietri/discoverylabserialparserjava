package parse;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.swing.JOptionPane;

import connect.SerialConnection;

public class UnitTester {

	InputStream in;
	OutputStream out;
	
	SerialParser parser;
	void test()
	{
		
		SerialConnection con = new SerialConnection();
		
		boolean done =false;
		String option = JOptionPane.showInputDialog("enter 'y' to connect");
		
		Trace t = new Trace(true);
		Trace valuesTrace = new Trace(true);
		
		
		//////////////////////////////////////////////////////////////////////////////////
		//Start unit test
		//////////////////////////////////////////////////////////////////////////////////
		if(option.equalsIgnoreCase("y"))
		{
		
			String port = JOptionPane.showInputDialog("Enter port");
			String speed = JOptionPane.showInputDialog("Enter speed");
				
			try
			{
				con.connect(port, Integer.parseInt(speed));
				this.in = con.in;
				this.out = con.out;
				parser = new SerialParser(in,out);
				parser.setTrace(false);
				read.start();
				write.start();
			}catch(Exception e)
			{
				System.out.println("error connecting");
				e.printStackTrace();
			}
		}else
		{
			SerialParser parser1 = new SerialParser();
			SerialParser parser2 = new SerialParser();
			while(!done)
			{
				try
				{
					//Get data
					String type = JOptionPane.showInputDialog("Choose type: i or s");
					String getKey = JOptionPane.showInputDialog("Enter a key");
					String input = JOptionPane.showInputDialog("Push data. Exit: -1");
					if(input.equalsIgnoreCase("-1"))
						break;
					char inputKey = ' ';
//					valuesTrace.trace("message = "+input);
					if(getKey.length() > 0)
						inputKey= getKey.charAt(0);
					
					
					//Is the command a string or integers
					if(type.equalsIgnoreCase("s"))
					{
						parser2.write(inputKey,input);
					}
					else if (type.equalsIgnoreCase("i"))
					{
						
						try{
							String tempInput = new String();
							StringTokenizer tok = new StringTokenizer(input,", ");
							int totalByteSum = 0;
							int[] temp = new int[tok.countTokens()];
							int index = 0 ; 
							while(tok.hasMoreTokens())
							{
								temp[index++] = Integer.parseInt(tok.nextToken());
							}
							 parser2.write(inputKey,temp);
						}catch(NumberFormatException e)
						{
							t.trace("Error: type integers separated by spaces: "+e);
						}catch(Exception e)
						{
							t.trace("error"+e);
						}
					}
					
					t.trace(input);
					
					////////////////////////////////////////////////////////////////////////////////////
					//start processing the input from parser2
					////////////////////////////////////////////////////////////////////////////////////
					
					parser1.read(input);
					String s = null;
					ArrayList<Integer> ints = null;
					
					if(parser1.isNextString())
					{
						//valuesTrace.trace("String: ");
						char key = parser1.getNextStringKey();
						if(key == 'l')
							valuesTrace.trace("This command is for the left ++\n");
						else if (key == 'r')
							valuesTrace.trace("This command is for the right --\n");
						String print = JOptionPane.showInputDialog("There are "+parser1.getStringMessagesCount()+" messages in the buffer. Type 1 to print one of the messages");
						if(print.equalsIgnoreCase("1"))
						{
							s = parser1.nextString();
							//valuesTrace.trace(" "+s+"\n");
						}
						
					}else if (parser1.isNextInteger() )
					{
						char key = parser1.getNextIntegerKey();
						if(key == 'l')
							valuesTrace.trace("This command is for the left ++\n");
						else if (key == 'r')
							valuesTrace.trace("This command is for the right --\n");
						String print = JOptionPane.showInputDialog("There are "+parser1.getIntegerValuesMessagesCount()+" messages in the buffer. Type 1 to print ln the messages");
						if(print.equalsIgnoreCase("1"))
						{
							ints = parser1.nextInteger();
							
							valuesTrace.trace("integers ");
							for(int i = 0 ; i < ints.size() ; i++ )
							{
								valuesTrace.trace(ints.get(i)+" ");
							}	
						}
					}
					valuesTrace.trace("\n");
				}catch(Exception e)
				{
					valuesTrace.trace("error on reading thread");
					e.printStackTrace();
				}
			}
		}
		
	}
	int getByteSum(String str)
	{
		int temp = 0;
		for(int i = 0 ; i < str.length() ; i++)
		{
			temp += (int)str.charAt(i);
		}
		return temp;
	}
	public void sendByte()
	{
		
	}
	
	Thread write = new Thread()
	{
		BufferedReader dataOut = new BufferedReader(new InputStreamReader(System.in));
		public void run()
		{
			Trace writeTrace = new Trace(true);
			char[] cbuf =new  char[300];
			int count = 2000;
			int[] testHand ={count, 2000,2000,2000,2000};
			int inc = 1;
			while(true)
			{
				try{
					
					/*
					 *Send to the device using the console 
					 * 
					 * 
					 */
					/*
					 * TEST THE HAND
					 */
					if(count < 2000)
						inc *= -1;
					else if ( count > 3900)
						inc *= -1;
					
					testHand[0] += inc;
					
					parser.write(testHand);
					
					if(dataOut.ready())
					{
						String data = dataOut.readLine();
						int[] array  = null;
						if(data.charAt(0)=='i' && data.charAt(1) == 'n')
						{
							data= data.substring(2);
							StringTokenizer tok = new StringTokenizer(data,", ");
							array = new int[tok.countTokens()];
							for(int i = 0 ; i < array.length ; i++)
							{
								array[i] = Integer.parseInt(tok.nextToken());
								
							}
							

							parser.write('r',array);
							
						}else{
						
						
						parser.write(data);
						
					
						//writeTrace.trace(data+"\n");
						}
					}
					
					write.sleep(1);
				}catch(Exception e)
				{
					writeTrace.trace("Error on writing thread");
				}
				
				
			}
			
			
		}
		
	};
	
	Thread read = new Thread()
	{
		Trace tr = new Trace(true);
		public void run()
		{
			
			
			while(true)
			{
				
				/*
				 * 
				 * get values from the device
				 * 
				 * 
				 */
				parser.read();
				try{
					
						//tr.trace("has next\n");
						
						
						String s = null;
						ArrayList<Integer> ints = null;
						if(parser.isNextString())
						{
							tr.trace("String =  ");
							s = parser.nextString();
							tr.trace(s+"\n");
							
						}else if ( parser.isNextInteger() )
						{
							
							ints = parser.nextInteger();
							//tr.trace("Integers =  ");
							for(int i = 0 ; i < ints.size() ; i++ )
							{
								tr.trace(ints.get(i)+" ");
							}
							tr.trace("\n");
							
						}
						//System.out.println();
						
					read.sleep(1);
				}catch(Exception e)
				{
					System.out.println("Error");
				}
			}
				
		}
	};
	
	
	public static void main(String[] args)
	{
		
		new UnitTester().test();
	}
}
