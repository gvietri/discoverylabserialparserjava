package parse;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

/*
 * protocol
 * <in>____<e/>
 * 
 * read and write message 
 * author: Giuseppe Vietri
 */

public class SerialParser 
{
	
	private InputStream in;
	public OutputStream out;
	private String nextString;						//this is the verified string that will go into the buffer
	private ArrayList<Integer> tempNextIntegers; 	//this are the verified values that will go into the buffer
	private char commandType;
	private int numberOfBytes;
	private int stage;
	private boolean newCommand;						//Indicates whether the the command is at the beggining or not
	private byte[] tempBuffer;		
	private Queue<IntegerMessage> IntegerBuffer;
	private Queue<StringMessage> stringBuffer;
	private int totalBytesSum;						//store the sum of incoming bytes to verify validity
	private int lastTwoBytesSum; 					//store the sum of the last two bytes and compare to commandSum
	private final char STRTYPE = 's';
	private final char INTTYPE = 'i';
	private final char ENDCHAR_1 = 'e';
	private final char ENDCHAR_2 = '/';
	private final char OPEN    = '<';
	private final char CLOSE   = '>';
	private final int  BYTESIZE = 128;
	private char commandKey;
	private byte[] tempIntegerByteHolder;
	////////
	//Debug
	//////
	private Trace writingTrace;
	private Trace t;
	private Trace readingTrace;
	private boolean isTraceOn;
	
	
	public SerialParser(InputStream in , OutputStream out)
	{
		this();
		this.in = in;
		this.out = out;
		
	}
	public  SerialParser()
	{
		
		this.IntegerBuffer = new LinkedList<IntegerMessage>();
		this.stringBuffer = new LinkedList<StringMessage>();
		this.newCommand = true;
		
		t = new Trace(false);
		readingTrace = new Trace(false);
		writingTrace = new Trace(false);
		
		tempIntegerByteHolder = new byte[2];
	}
	public void setTrace(boolean tr)
	{
		t.setTrace(tr);
	}
	//call this to supply data to be process
	public void read(String stream)
	{
		byte[] b = stream.getBytes();
		for(byte x : b)
			read(x);
	}
	public void read(byte[] stream)
	{
		
		for(byte x : stream)
			read(x);
	}
	//Call this when the client has supplied an inputStream
	public void read()
	{
		this.tempBuffer = new byte[255];
		if(this.in != null && this.out != null)
		{
			try
			{
				if(this.in.available() > 0)
				{
					int size = this.in.read(this.tempBuffer);
					for(int i =0; i < size ; i++)
					{
						read(this.tempBuffer[i]);
					}
				}
			}catch(Exception e)
			{
				
				e.printStackTrace();
			}
		}else
			readingTrace.trace("input/output stream is empty");
	}
	
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////
	
	
	//read and validate incoming messages 
	//uses stages to validate that each byte one at a time
	private void read(byte input)
	{
		try
		{
			
			char c = (char)input;
			boolean error = false;
			
			switch(this.stage)
			{
			case 0:
				t.trace("\nstage: ("+c+") "+stage+"\n");
				if(c == OPEN && this.newCommand)
					this.stage++;
				else 
					error = true;
				this.totalBytesSum = 0;
				this.lastTwoBytesSum = 0;
				break;
			case 1:														//check the type
				t.trace("stage: ("+c+") "+stage+"");
				
				if(c == this.INTTYPE)
				{
					this.commandType = (char)c;
					this.tempNextIntegers = new ArrayList<Integer>();
					this.stage++;
					t.trace(" Integer created");
				}
				else if(c == this.STRTYPE)
				{
					this.commandType = (char)c;
					this.nextString = new String();
					this.stage++;
					t.trace(" String created");
				}else
					error = true;
				
				t.trace("\n");
				break;
			case 2 :
				t.trace("stage: ("+c+") "+stage+"\n");
				commandKey = c;									//get a key to assign command to different purposes 
				this.stage++;
				break;
			case 3:												//How many bytes
				t.trace("stage: ("+c+") "+stage);
				
				this.numberOfBytes = (int)c;
				
				this.stage++;
				t.trace(" command size = "+this.numberOfBytes+"\n");
				break;
			case 4:												//after this start reading the bytes
				t.trace("stage: ("+c+") "+stage+"\n");
				
				if(c == CLOSE)
				{
					this.newCommand = false;
					this.stage++;
				}else
					error = true;
				
				break;
			case 5:
				
				t.trace("stage: ("+c+") "+stage+": ");
//				this.totalBytesSum +=c & 0xFF;
				this.totalBytesSum +=(int) input;
				if(this.commandType == this.INTTYPE)
				{
//					int tempInt =c & 0xFF;
					int tempInt =(int) input;
					t.trace(tempInt+"\n");
					//get the value out of two bytes.
					//first calculate the value of the first byte and store it
					//second calculate the value of the second byte and added to the location of the firs byte
					if(this.numberOfBytes % 2 == 0 )
					{
						int valueOfFirstByte = tempInt*(BYTESIZE-1);
						this.tempNextIntegers.add(valueOfFirstByte);
						tempIntegerByteHolder[0] = input;
						//t.trace("\tvalues of first byte = ("+tempInt+") "+valueOfFirstByte+"\n");
						t.trace("\t\t");
					}else
					{
						
						int index = this.tempNextIntegers.size()-1;
//						int temp = this.tempNextIntegers.get(index);
//						this.tempNextIntegers.set(index, temp + tempInt);
						
						//NEW inplementation
						tempIntegerByteHolder[1] = input;
						int value = (int)ByteBuffer.wrap(this.tempIntegerByteHolder).getShort();
						this.tempNextIntegers.set(index, value);
						
						t.trace("\t value = "+this.tempNextIntegers.get(index) +" index = "+index);
					}
					//t.trace("\n");
				}else if(this.commandType == this.STRTYPE)
				{
					//Add all the bytes to a string
					//TODO may need to change to something more efficient
					this.nextString += c;
				}
				
				t.trace("  command lenght = "+this.numberOfBytes+"\n");
				
				if(--this.numberOfBytes <= 0)
					stage++;
				break;
				
			case 6 :		//Check sum stage
				//for stage 5 and 6 get the value of the last two bytes and store it
				t.trace("stage: ("+c+") "+stage+"\n");
//				int temp1 = c & 0xFF;
				int temp1 = (int)input;
				this.lastTwoBytesSum =temp1*(BYTESIZE -1); 
				this.stage++;
				break;
			case 7 :		//Check sum stage
				t.trace("stage: ("+c+") "+stage+"\n");
//				int temp2 =   c & 0xFF;
				int temp2 = (int)input;
				this.lastTwoBytesSum += temp2;
				this.stage++;
				break;
			case 8:
				//Start closing the message
				t.trace("stage: ("+c+") "+stage+"\n");
				if(c == OPEN)
					stage++;
				else
					error = true;
				break;
			case 9:
				t.trace("stage: ("+c+") "+stage+"\n");
				if(c == this.ENDCHAR_1)
					stage++;
				else
					error = true;
				break;
			case 10:
				t.trace("stage: ("+c+") "+stage+"\n");
				if(c ==ENDCHAR_2)
					stage++;
				else
					error = true;
				break;
			case 11:
				//End of message
				t.trace("stage: ("+c+") "+stage+"\n");
				if(c == CLOSE)
				{
					addMessageToBuffer();
					stage = 0;
					this.newCommand = true;
				}
				else
					error = true;
			}
			//If anything goes wrong. Start over
			if(error)
			{
				t.trace("error ("+c+")  \n");
				this.stage = 0;
				this.newCommand = true;
			}
		}catch(Exception e)
		{
			this.stage = 0;
			this.newCommand = true;
			e.printStackTrace();
		}
			
	}
	//Stores the validated messages into queue to be use later
	private void addMessageToBuffer()
	{
//		if(this.lastTwoBytesSum == this.totalBytesSum)
		if(true)
		{
			if(this.commandType == this.INTTYPE)
			{
				IntegerMessage msg = new IntegerMessage(commandKey,tempNextIntegers);
				//this.IntegerBuffer.offer(tempNextIntegers);
				this.IntegerBuffer.offer(msg);
				t.trace("Integer added to buffer\n");
			}else if(this.commandType == this.STRTYPE)
			{
				StringMessage msg = new StringMessage(commandKey,nextString);
				this.stringBuffer.offer(msg);
				t.trace("String added to buffer\n");
			}
		}
//		else
//		{
//			if(this.tempNextIntegers != null)
//				for(int x : tempNextIntegers)
//					t.trace(x +" ");
//			t.trace("\nBytes sum error "+this.totalBytesSum+" "+this.lastTwoBytesSum+"\n");
//		}
	}
	//Send bytes 
	private byte[] writeMessageType(char key, byte[] argStr, char type) 
	{
		
		
		int size = argStr.length;
		int commandSum = this.getByteSum(argStr);
	
		char sizeChar = (char) size;
//		byte byte1 = (byte) (commandSum / (BYTESIZE -1 ));
//		byte byte2 = (byte) (commandSum % (BYTESIZE - 1));
		
		
		short tempCommandSum = (short)commandSum;
		byte[] tempbytes = ByteBuffer.allocate(2).putShort(tempCommandSum).array();
		
		
		byte byte1 = tempbytes[0];
		byte byte2 = tempbytes[1];
		
		ArrayList<Byte> str = new ArrayList<Byte>();
		str.add((byte)OPEN);
		str.add((byte)type);
		str.add((byte)key);
		str.add((byte)sizeChar);
		str.add((byte)CLOSE);
		for(int i = 0 ; i < size ; i++)
		{
			byte temp = (byte)argStr[i];
			str.add(temp);
			writingTrace.trace(i+": "+temp+"\n");
		}
		writingTrace.trace("/////////////////////////////////////////////////////////////\n");
		str.add((byte)byte1);
		str.add((byte)byte2);
		str.add((byte)OPEN);
		str.add((byte)ENDCHAR_1);
		str.add((byte)ENDCHAR_2);
		str.add((byte)CLOSE);
		int strSize = str.size();
		byte[] messagePackage = new byte[strSize];
		//writingTrace.trace("Setting bytes!!!!\n");
		
		
		if(this.out != null)
		{
			for(int i = 0 ; i < strSize ; i++)
			{
				try{
				messagePackage[i] = str.get(i);
				//writingTrace.trace(messagePackage[i]+"\n");
				this.out.write(messagePackage[i]);
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		}else
		{
			for(int i = 0 ; i < strSize ; i++)
			{
				messagePackage[i] = str.get(i);
				//writingTrace.trace(messagePackage[i]+"\n");
				
			
			}
		}
		
		return messagePackage;
	}
	
	public void write(String values) 
	{
		char t = 1;
		write(t, values);
	}
	public void  write(char key,String argStr) 
	{
		 writeMessageType(key,argStr.getBytes(), this.STRTYPE);
	}
	public void write(int[] values) 
	{
		char t = 1;
		 write(t, values);
	}
	public byte[] write(char key,int[] values)
	{
		
		byte[] bytes = new byte[values.length*2];
		//add bytes to string
		int byteIndex = 0;
		for(int i = 0 ; i < values.length ; i++)
		{
			short tempValues = (short) values[i] ;
			byte[] tempbytes = ByteBuffer.allocate(2).putShort(tempValues).array();
			bytes[byteIndex++] = tempbytes[0];
			bytes[byteIndex++] = tempbytes[1];
			
//			int tempByte = values[i] / (BYTESIZE - 1);
//			if(tempByte == 0)
//				bytes[byteIndex++] = 0;
//			else
//				bytes[byteIndex++] = (byte)(tempByte);
//			
//				bytes[byteIndex++] = (byte)(values[i]  % (BYTESIZE - 1));
	
		}
		return writeMessageType(key, bytes, this.INTTYPE);
	}
	
	//Use to encode outgoing data
	//count the bytes in one command
	private int getByteSum(byte[] str)
	{
		int temp = 0;
		for(int i = 0 ; i < str.length ; i++)
		{
			temp += (int)str[i];
		}
		return temp;
	}
	
	/*
	 * get string and integer buffer information
	 */
	public boolean isNextString()
	{
		return stringBuffer.peek() != null;
	}
	public int getStringMessagesCount()
	{
		if(this.stringBuffer == null)return 0;
		return this.stringBuffer.size();
	}
	public char getNextStringKey()
	{
		return stringBuffer.peek().key;
	}
	public String nextString() 
	{	
		String next = stringBuffer.poll().data;
		
		return next;
	}
	public boolean isNextInteger()
	{
		return IntegerBuffer.peek() != null;
	}
	public int getIntegerValuesMessagesCount()
	{
		if(this.IntegerBuffer == null)return 0;
		return this.IntegerBuffer.size();
	}
	public ArrayList<Integer> nextInteger() 
	{
		return IntegerBuffer.poll().data;
	}
	public char getNextIntegerKey()
	{
		return IntegerBuffer.peek().key;
	}
	
	class StringMessage
	{
		public char key;
		public String data;
		public StringMessage(char k, String d)
		{
			this.key = k;
			this.data = d;
		}
	}
	class IntegerMessage
	{
		public char key;
		public ArrayList<Integer> data;
		public IntegerMessage(char k, ArrayList<Integer> d)
		{
			this.key = k;
			this.data = d;
		}
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////
	//TODO complete class
	static class Data
	{
		static public Map<Character,Queue<ArrayList<Object>>> data = new HashMap<Character,Queue<ArrayList<Object>>>();
		
		
		public static void addMessage(char argType, ArrayList<Object> list)
		{
			ArrayList<Object> tempList = new ArrayList<Object>();
			tempList.addAll(list);
			if(data.containsKey(argType))
			{
				Queue<ArrayList<Object>> dataBuffer = data.get(argType);
				dataBuffer.offer(tempList);
				
			}else
			{
				Queue<ArrayList<Object>> dataBuffer =new LinkedList<ArrayList<Object>>();
				dataBuffer.offer(tempList);
				data.put(argType, dataBuffer);
				
			}
		
		}
		private void addToBuffer()
		{
			
		}
		
		public static ArrayList<Object> getValues(char argType)
		{
			Queue<ArrayList<Object>> dataBuffer = data.get(argType);
			return dataBuffer.poll();
			
		}
		
	}


	
	
	
	
}