package connect;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;

import java.io.InputStream;
import java.io.OutputStream;

//import connect.Serial.SerialReader;
//import connect.Serial.SerialWriter;


public class SerialConnection {
	public InputStream in;
	public OutputStream out;
	
	public void connect(String portName,int baud) throws Exception
	{
		try{
		
		 	CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(portName);
		 	
	        if ( portIdentifier.isCurrentlyOwned() )
	        {
	        	
	            System.out.println("Error: Port is currently in use");
	        }
	        else
	        {
	            CommPort commPort = portIdentifier.open(this.getClass().getName(),2000);
			
	            
	            if ( commPort instanceof SerialPort )
	            {
	                SerialPort serialPort = (SerialPort) commPort;
	                serialPort.setSerialPortParams(baud,SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);
	                
	                in = serialPort.getInputStream();
	                out = serialPort.getOutputStream();
	                System.out.println("connected");

	            }
	            else
	            {
	                System.out.println("Error: Only serial ports are handled by this example.");
	            }
	        }
		}catch(Exception e)
		{
			System.out.println("Exception cought trying to identify the port");
			e.printStackTrace();
		}
	}

	
}
